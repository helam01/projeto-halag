def traca_reta(x0,y0, x1,y1) :
    vX = []
    vY = []
    result = []

    currentX = x0
    currentY = y0

    loop = True
    while loop:
        if currentX == x1 and currentY == y1:
            loop = False
            
        vX.append(currentX)
        vY.append(currentY)
        
        if currentX < x1 :
            currentX = currentX+1
        elif currentX > x1 :
            currentX = currentX-1

        if currentY < y1 :
            currentY = currentY+1
        elif currentY > y1 :
            currentY = currentY-1
    
    result.append( vX )
    result.append( vY )
    
    return result


points = traca_reta(6,4,1,5)
print( points )
